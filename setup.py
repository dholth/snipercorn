#!/usr/bin/env python
# Call enscons to emulate setup.py, installing enscons if necessary.

import subprocess
import sys

sys.path[0:0] = ["setup-requires"]

try:
    import enscons.setup
except ImportError:
    requires = ["scons>=3.0.5", "enscons>=0.12"]
    subprocess.check_call(
        [sys.executable, "-m", "pip", "install", "-t", "setup-requires"] + requires
    )
    del sys.path_importer_cache["setup-requires"]  # needed if setup-requires was absent
    import enscons.setup

enscons.setup.setup()
