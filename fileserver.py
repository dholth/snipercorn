#!/usr/bin/env -S /usr/bin/authbind python
"""
Serve static files.
"""

import os
import os.path
import asyncio
from hypercorn.asyncio import serve
from snipercorn.sni import SNIConfig

from quart import Quart, send_from_directory
from quart.exceptions import NotFound

app = Quart(__name__)

ROOT_PATH = os.path.expanduser("~/public_html")


@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
async def send(path=None):
    try:
        return await send_from_directory(ROOT_PATH, path)
    except NotFound:
        return await send_from_directory(ROOT_PATH, os.path.join(path, "index.html"))


if __name__ == "__main__":
    # bind can be a list of strings or a string
    config = SNIConfig.from_mapping({"bind": ":::443"})

    asyncio.run(serve(app, config))

